package com.example.featureflags.acceptance;

import com.example.featureflags.core.model.Country;
import com.example.featureflags.utils.AbstractFeatureRepositorySupport;
import com.example.featureflags.utils.FeatureFactory;
import io.restassured.RestAssured;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FeatureToggleApiAcceptanceTests extends AbstractFeatureRepositorySupport {

    @LocalServerPort
    private int port;

    @Before
    public void setUp() {
        RestAssured.port = port;
    }

    private final String featureName1 = "feature-1";
    private final String featureName2 = "feature-2";
    private final Country country = new Country("es");

    @Test
    public void it_should_get_all_features() {
        givenAFeature(FeatureFactory.aActiveFeature(featureName1));
        givenAFeature(FeatureFactory.aActiveFeature(featureName2, country));
        when().get("/v1/features")
                .then()
                .statusCode(HttpStatus.OK.value())
                .body("$.size", equalTo(2));
    }

    @Test
    public void it_should_get_no_features() {
        when().get("/v1/features")
                .then()
                .statusCode(HttpStatus.OK.value())
                .body("$", hasSize(0));
    }
}
