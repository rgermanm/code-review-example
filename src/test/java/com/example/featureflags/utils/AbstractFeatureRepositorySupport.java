package com.example.featureflags.utils;

import com.example.featureflags.core.model.Feature;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.List;

public abstract class AbstractFeatureRepositorySupport {

    @Autowired
    @Qualifier("features")
    protected List<Feature> features;

    @Before
    public void setupFeatureRepository() {

        features.clear();
    }

    protected Feature givenAFeature(Feature feature) {
        features.add(feature);
        return feature;
    }
}
