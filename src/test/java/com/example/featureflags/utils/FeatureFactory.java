package com.example.featureflags.utils;

import com.example.featureflags.core.model.Country;
import com.example.featureflags.core.model.Feature;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FeatureFactory {
    private static final String DEFAULT_DESCRIPTION = "I'm a default description";


    public static Feature aActiveFeature(String name, Country... country) {
        return new Feature(name,
                DEFAULT_DESCRIPTION,
                Boolean.TRUE,
                Arrays.asList(country)
        );
    }

    public static Feature aActiveFeature(String name) {
        return new Feature(name,
                DEFAULT_DESCRIPTION,
                Boolean.TRUE,
                emptyList()
        );
    }

    private final static List emptyList() {
        return Collections.emptyList();
    }
}
