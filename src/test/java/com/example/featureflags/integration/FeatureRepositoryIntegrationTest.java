package com.example.featureflags.integration;

import com.example.featureflags.core.model.Country;
import com.example.featureflags.core.model.Feature;
import com.example.featureflags.infrastructure.repository.FeatureRepository;
import com.example.featureflags.utils.AbstractFeatureRepositorySupport;
import com.example.featureflags.utils.FeatureFactory;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FeatureRepositoryIntegrationTest extends AbstractFeatureRepositorySupport {

    private final Country countryEs = new Country("es");
    private final Country countryAr = new Country("ar");

    @Autowired
    FeatureRepository featureRepository;

    @Test
    public void it_should_get_all_features(){
        final Feature feature1 = givenAFeature(FeatureFactory.aActiveFeature("feature-1"));
        final Feature feature2 = givenAFeature(FeatureFactory.aActiveFeature("feature-2", countryEs));
        final Feature feature3 = givenAFeature(FeatureFactory.aActiveFeature("feature-3", countryEs, countryAr));

        List<Feature> features = featureRepository.findAll();

        assertThat(features, Matchers.containsInAnyOrder(feature1, feature2, feature3));
    }

    @Test
    public void it_should_get_no_features(){
        List<Feature> features = featureRepository.findAll();

        assertThat(features, Matchers.empty());
    }
}
