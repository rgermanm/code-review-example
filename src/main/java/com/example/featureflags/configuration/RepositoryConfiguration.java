package com.example.featureflags.configuration;

import com.example.featureflags.core.model.Country;
import com.example.featureflags.core.model.Feature;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@Configuration
public class RepositoryConfiguration {

    @Bean("features")
    public List<Feature> features() {
        return new LinkedList<>(Arrays.asList(
                new Feature("feature-1",
                        "I'm a feature-1",
                        true,
                        Arrays.asList(new Country("be"))),
                new Feature("feature-2",
                        "I'm a feature-2",
                        true,
                        Arrays.asList(new Country("es"))),
                new Feature("feature-3",
                        "I'm a feature-3",
                        true,
                        Arrays.asList(new Country("be"), new Country("es")))
        ));
    }
}
