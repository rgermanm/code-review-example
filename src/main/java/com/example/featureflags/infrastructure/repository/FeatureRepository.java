package com.example.featureflags.infrastructure.repository;

import com.example.featureflags.core.model.Feature;
import com.example.featureflags.core.usecase.Features;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FeatureRepository implements Features {
    private final List<Feature> featureList;

    public FeatureRepository(@Qualifier("features") List<Feature> featureList) {
        this.featureList = featureList;
    }

    @Override
    public List<Feature> findAll() {
        return featureList;
    }
}
