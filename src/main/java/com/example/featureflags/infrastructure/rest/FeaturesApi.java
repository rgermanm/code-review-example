package com.example.featureflags.infrastructure.rest;

import com.example.featureflags.core.model.Feature;
import com.example.featureflags.core.usecase.FeatureService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/v1/features")
public class FeaturesApi {
    private final FeatureService featureService;

    public FeaturesApi(FeatureService featureService) {
        this.featureService = featureService;
    }

    @GetMapping()
    public List<Feature> getFeatures() {
       return featureService.findAll();
    }
}
