package com.example.featureflags.core.model;

import lombok.Value;

import java.util.List;

@Value
public class Feature {
    String name;
    String description;
    Boolean active;
    List<Country> countryList;
}
