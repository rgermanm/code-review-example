package com.example.featureflags.core.usecase;

import com.example.featureflags.core.model.Feature;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FeatureService {
    private final Features features;

    public FeatureService(Features features) {
        this.features = features;
    }

    public List<Feature> findAll() {
        return features.findAll();
    };
}
