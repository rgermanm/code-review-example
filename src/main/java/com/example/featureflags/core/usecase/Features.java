package com.example.featureflags.core.usecase;

import com.example.featureflags.core.model.Feature;

import java.util.List;

public interface Features {
    List<Feature> findAll();
}
